-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Gép: localhost
-- Létrehozás ideje: 2021. Máj 21. 11:16
-- Kiszolgáló verziója: 8.0.18
-- PHP verzió: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `jo_kis_java`
--

DELIMITER $$
--
-- Eljárások
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `create_movie` (IN `titleIN` VARCHAR(45), IN `lengthIN` INT, IN `genreIN` VARCHAR(45))  NO SQL
BEGIN
	INSERT INTO `movie`(`movie`.`Title`,`movie`.`Length`,`movie`.`Genre`,`movie`.`IsActive`) VALUES(titleIN,lengthIN,genreIN,1);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_movie_character` (IN `namein` VARCHAR(45), IN `movienameIN` VARCHAR(45))  NO SQL
BEGIN
	CALL get_movie_id_by_title(movienameIN,@a);
	INSERT INTO `character`(`character`.`Name`,`character`.`Movie_id`,`character`.`IsActive`) VALUES(namein,@a,1);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_movie_character_rating` (IN `usernameIN` VARCHAR(45), IN `charcternameIN` VARCHAR(45), IN `movienameIN` VARCHAR(45), IN `ratingIN` INT)  NO SQL
BEGIN
	CALL get_movie_id_by_title(movienameIN,@a);
    CALL get_user_id_by_name(usernameIN,@b);
    CALL get_character_id_by_name(charcternameIN,@c);
	INSERT INTO `rating`(`rating`.`User_id`,`rating`.`Character_id`,`rating`.`Movie_id`,`rating`.`Rating`,`rating`.`IsActive`) VALUES(@b,@c,@a,ratingIN,1);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_movie_rating` (IN `usernameIN` VARCHAR(45), IN `movienameIN` VARCHAR(45), IN `ratingIN` INT)  NO SQL
BEGIN
	CALL get_movie_id_by_title(movienameIN,@a);
    CALL get_user_id_by_name(usernameIN,@b);
	INSERT INTO `rating`(`rating`.`User_id`,`rating`.`Movie_id`,`rating`.`Rating`,`rating`.`IsActive`) VALUES(@b,@a,ratingIN,1);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_series` (IN `titleIN` VARCHAR(1000), IN `episodeIN` INT, IN `lengthIN` INT, IN `genreIN` VARCHAR(45))  NO SQL
BEGIN
	INSERT INTO `series`(`series`.`Title`,`series`.`Episode`,`series`.`Length`,`series`.`Genre`,`series`.`IsActive`)VALUES(titleIN,episodeIN,lengthIN,genreIN,1);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_series_character` (IN `nameIN` VARCHAR(45), IN `seriesnameIN` VARCHAR(45))  NO SQL
BEGIN
	CALL get_series_id_by_title(seriesnameIN,@a);
	INSERT INTO `character`(`character`.`Name`,`character`.`Series_id`,`character`.`IsActive`) VALUES(nameIN,@a,1);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_series_character_rating` (IN `usernameIN` VARCHAR(45), IN `charcternameIN` VARCHAR(45), IN `seriesnameIN` VARCHAR(45), IN `ratingIN` INT)  NO SQL
BEGIN
	CALL get_series_id_by_title(seriesnameIN,@a);
    CALL get_user_id_by_name(usernameIN,@b);
    CALL get_character_id_by_name(charcternameIN,@c);
	INSERT INTO `rating`(`rating`.`User_id`,`rating`.`Character_id`,`rating`.`Series_id`,`rating`.`Rating`,`rating`.`IsActive`) VALUES(@b,@c,@a,ratingIN,1);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_series_rating` (IN `usernameIN` VARCHAR(45), IN `seriesnameIN` VARCHAR(45), IN `ratingIN` INT)  NO SQL
BEGIN
	CALL get_series_id_by_title(seriesnameIN,@a);
    CALL get_user_id_by_name(usernameIN,@b);
	INSERT INTO `rating`(`rating`.`User_id`,`rating`.`Series_id`,`rating`.`Rating`,`rating`.`IsActive`) VALUES(@b,@a,ratingIN,1);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_user` (IN `usernameIN` VARCHAR(45), IN `passwdIN` VARCHAR(45), IN `emailIN` VARCHAR(254))  NO SQL
BEGIN
INSERT INTO `user`(`user`.`Username`,`user`.`Password`,`user`.`Email`,`user`.`IsActive`) VALUES(usernameIN,SHA1(passwdIN),emailIN,1);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_character_by_name` (IN `nameIN` VARCHAR(45))  NO SQL
BEGIN
	Call get_character_id_by_name(nameIN,@a);
	UPDATE `character` SET `character`.`IsActive` = 0 WHERE `character`.`id` = @a;
    UPDATE `rating` SET `rating`.`IsActive` = 0 WHERE `rating`.`Character_id` = @a;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_movie_by_title` (IN `nameIN` VARCHAR(45))  NO SQL
BEGIN
	Call get_movie_id_by_title(nameIN,@a);
	UPDATE `movie` SET `movie`.`IsActive` = 0 WHERE `movie`.`id` = @a;
    UPDATE `rating` SET `rating`.`IsActive` = 0 WHERE `rating`.`Movie_id` = @a;
    
    UPDATE `character` SET `character`.`IsActive` = 0 WHERE `character`.`Movie_id`=@a;
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_rating_by_id` (IN `idIN` TINYINT)  NO SQL
BEGIN
	UPDATE `rating` SET `rating`.`IsActive` = 0 WHERE `rating`.`id`=idIN;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_series_by_title` (IN `nameIN` VARCHAR(45))  NO SQL
BEGIN
	Call get_series_id_by_title(nameIN,@a);
    
	UPDATE `series` SET `series`.`IsActive` = 0 WHERE `series`.`id` = @a;
    UPDATE `rating` SET `rating`.`IsActive` = 0 WHERE `rating`.`Series_id` = @a;
    
    UPDATE `character` SET `character`.`IsActive` = 0 WHERE `character`.`Series_id` = @a;
    
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_user_by_name` (IN `nameIN` VARCHAR(45))  NO SQL
BEGIN
	CALL get_user_id_by_name(nameIN,@a);
	UPDATE `user` SET `user`.`IsActive` = 0 WHERE `user`.`Username` = nameIN;
    UPDATE `rating` SET `rating`.`IsActive`= 0 WHERE `rating`.`User_id`=@a;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_character` ()  NO SQL
SELECT `character`.`id`,`character`.`Name` FROM `character` WHERE `character`.`IsActive`=1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_movie_title` ()  NO SQL
SELECT `movie`.`id`, `movie`.`Title`, `movie`.`Length`, `movie`.`Genre` FROM `movie` WHERE `movie`.`IsActive`=1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_rating_by_all_character` ()  NO SQL
BEGIN
	SELECT `rating`.`Character_id`, `character`.`Name`, rating.Rating FROM rating 
INNER JOIN
    `character` ON rating.Character_id = `character`.`id`;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_rating_by_character` (IN `nameIN` VARCHAR(45))  NO SQL
BEGIN
	Call get_character_id_by_name(nameIN,@a);
    SELECT `rating`.`Rating` FROM `rating` WHERE `rating`.`Character_id`=@a AND `rating`.`IsActive`= 1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_rating_by_movie` (IN `nameIN` VARCHAR(45))  NO SQL
BEGIN
	Call get_movie_id_by_title(nameIN,@a);
    SELECT `rating`.`Rating` FROM `rating` WHERE `rating`.`Movie_id`=@a AND `rating`.`IsActive`= 1 AND `rating`.`Character_id` IS NULL;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_rating_by_series` (IN `nameIN` VARCHAR(45))  NO SQL
BEGIN
	Call get_series_id_by_title(nameIN,@a);
    SELECT `rating`.`Rating` FROM `rating` WHERE `rating`.`Series_id`=@a AND `rating`.`IsActive`= 1 AND `rating`.`Character_id` IS NULL;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_series_title` ()  NO SQL
SELECT `series`.`id`,`series`.`Title`,`series`.`Episode`,`series`.`Length`,`series`.`Genre` FROM `series` WHERE `series`.`IsActive`=1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_character_by_id` (IN `idIN` INT, OUT `nameOUT` VARCHAR(45))  NO SQL
SELECT `character`.`Name` INTO nameOUT FROM `character` WHERE `character`.`id`=idIN AND `character`.`IsActive`=1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_character_id_by_name` (IN `nameIN` VARCHAR(45), OUT `idOut` INT)  NO SQL
SELECT `character`.`id` INTO idOut FROM `character` WHERE `character`.`Name` = nameIN AND `character`.`IsActive` = 1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_character_id_by_series_id` (IN `seriesidin` INT, OUT `characteridout` INT)  NO SQL
SELECT `character`.`id` INTO characteridout FROM `character` WHERE `character`.`Series_id`=seriesidin$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_movie_id_by_title` (IN `titleIN` VARCHAR(45), OUT `idOut` INT(1))  NO SQL
SELECT `movie`.`id` INTO idOut FROM `movie` WHERE `movie`.`Title` = titleIN AND `movie`.`IsActive`=1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_series_id_by_title` (IN `titleIN` VARCHAR(45), OUT `idOut` INT)  NO SQL
SELECT `series`.`id` INTO idOut FROM `series` WHERE `series`.`Title` = titleIN AND `series`.`IsActive`=1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_by_email` (IN `emailIN` VARCHAR(45), OUT `nameOUT` VARCHAR(45))  NO SQL
BEGIN
SELECT `user`.`Username` INTO nameOUT FROM `user` WHERE `user`.`Email` = emailIN AND `user`.`IsActive`=1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_id_by_name` (IN `nameIN` VARCHAR(45), OUT `useridOUT` INT)  NO SQL
BEGIN
SELECT `user`.`id` INTO useridOUT FROM `user` WHERE `user`.`Username` = nameIN AND `user`.`IsActive`=1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `join` ()  NO SQL
UPDATE rating
INNER JOIN
    `character` ON rating.Character_id = `character`.`id`
SET 
    rating.IsActive=0 WHERE `character`.`IsActive`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_character_rating_by_name_and_user` (IN `nameIN` VARCHAR(45), IN `newratingIN` INT, IN `usernameIN` VARCHAR(45))  NO SQL
BEGIN
CALL get_character_id_by_name(nameIN,@a);
CALL get_user_id_by_name(usernameIN,@b);
UPDATE `rating` SET `rating`.`Rating`=newratingIN WHERE `rating`.`Character_id`=@a AND `rating`.`User_id`=@b;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_movie` (IN `idIN` INT, IN `titleIN` VARCHAR(45), IN `lengthIN` INT, IN `genreIN` VARCHAR(45))  NO SQL
BEGIN
UPDATE `movie` SET `movie`.`Title`=titleIN,`movie`.`Length`=lengthIN, `movie`.`Genre`=genreIN WHERE `movie`.`id`=idIN;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_movie_character_by_name` (IN `nameIN` VARCHAR(45), IN `movieIN` VARCHAR(45), IN `newnameIN` VARCHAR(45))  NO SQL
BEGIN
CALL get_character_id_by_name(nameIN,@a);
CALL get_movie_id_by_title(movieIN,@b);
UPDATE `character` SET `character`.`Name`= newnameIN,`character`.`Movie_id`=@b WHERE `character`.`id`=@a;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_movie_rating_by_name_and_user` (IN `nameIN` VARCHAR(45), IN `newratingIN` INT, IN `usernameIN` VARCHAR(45))  NO SQL
BEGIN
CALL get_movie_id_by_title(nameIN,@a);
CALL get_user_id_by_name(usernameIN,@b);
UPDATE `rating` SET `rating`.`Rating`=newratingIN WHERE `rating`.`Movie_id`=@a AND `rating`.`User_id`=@b AND `rating`.`Character_id`IS NULL;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_series` (IN `idIN` INT, IN `titleIN` VARCHAR(45), IN `episodeIN` INT, IN `lengthIN` INT, IN `genreIN` VARCHAR(45))  NO SQL
UPDATE `series` SET `series`.`Title`=titleIN,`series`.`Episode`=episodeIN,`series`.`Length`=lengthIN,`series`.`Genre`=genreIN WHERE `series`.`id`=idIN$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_series_character_by_name` (IN `nameIN` VARCHAR(45), IN `seriesIN` VARCHAR(45), IN `newnameIN` VARCHAR(45))  NO SQL
BEGIN
CALL get_character_id_by_name(nameIN,@a);
CALL get_series_id_by_title(seriesIN,@b);
UPDATE `character` SET `character`.`Name`=newnameIN,`character`.`Series_id`=@b WHERE `character`.`id`=@a;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_series_rating_by_name_and_user` (IN `nameIN` VARCHAR(45), IN `newratingIN` INT, IN `usernameIN` VARCHAR(45))  NO SQL
BEGIN
CALL get_series_id_by_title(nameIN,@a);
CALL get_user_id_by_name(usernameIN,@b);
UPDATE `rating` SET `rating`.`Rating`=newratingIN WHERE `rating`.`Series_id`=@a AND `rating`.`User_id`=@b AND `rating`.`Character_id`IS NULL;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user` (IN `usernameIN` VARCHAR(45), IN `passwdIN` VARCHAR(45), IN `emailIN` VARCHAR(254))  NO SQL
BEGIN
UPDATE `user` SET `user`.`Password`=SHA1(passwdIN), `user`.`Email`=emailIN WHERE `user`.`Username`=usernameIN;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `character`
--

CREATE TABLE `character` (
  `id` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Movie_id` int(11) DEFAULT NULL,
  `Series_id` int(11) DEFAULT NULL,
  `IsActive` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `character`
--

INSERT INTO `character` (`id`, `Name`, `Movie_id`, `Series_id`, `IsActive`) VALUES
(5, 'Bruce', 3, NULL, 1),
(6, 'Jonas', NULL, 3, 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `movie`
--

CREATE TABLE `movie` (
  `id` int(11) NOT NULL,
  `Title` varchar(45) NOT NULL,
  `Length` int(45) NOT NULL,
  `Genre` varchar(45) NOT NULL,
  `IsActive` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `movie`
--

INSERT INTO `movie` (`id`, `Title`, `Length`, `Genre`, `IsActive`) VALUES
(1, 'Tesztfilm1', 15, 'TTeszt', 0),
(2, 'Teszt film2', 2, 'Teszt', 1),
(3, 'Bosszuallok', 180, 'Akcio', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `rating`
--

CREATE TABLE `rating` (
  `id` int(11) NOT NULL,
  `User_id` int(11) NOT NULL,
  `Character_id` int(11) DEFAULT NULL,
  `Movie_id` int(11) DEFAULT NULL,
  `Series_id` int(11) DEFAULT NULL,
  `Rating` int(11) NOT NULL,
  `IsActive` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `rating`
--

INSERT INTO `rating` (`id`, `User_id`, `Character_id`, `Movie_id`, `Series_id`, `Rating`, `IsActive`) VALUES
(7, 3, 5, 3, NULL, 5, 1),
(8, 3, NULL, NULL, 3, 8, 1),
(9, 3, NULL, NULL, 3, 10, 1),
(10, 3, NULL, 3, NULL, 8, 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `series`
--

CREATE TABLE `series` (
  `id` int(11) NOT NULL,
  `Title` varchar(45) NOT NULL,
  `Episode` int(11) NOT NULL,
  `Length` int(45) NOT NULL,
  `Genre` varchar(45) NOT NULL,
  `IsActive` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `series`
--

INSERT INTO `series` (`id`, `Title`, `Episode`, `Length`, `Genre`, `IsActive`) VALUES
(1, 'tesztsori', 1, 15, 'teszt', 0),
(2, 'Tesztsori2', 1, 15, 'teszt2', 0),
(3, 'Dark', 15, 59, 'Misztikus', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `Username` varchar(45) NOT NULL,
  `Password` varchar(45) NOT NULL,
  `Email` varchar(254) NOT NULL,
  `IsActive` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `user`
--

INSERT INTO `user` (`id`, `Username`, `Password`, `Email`, `IsActive`) VALUES
(3, 'GipszJakab', '54e8070e4c90fa2c728b922423869a87afb1af01', 'GiszJakab12@gmail.com', 1);

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `character`
--
ALTER TABLE `character`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Character_Movie1_idx` (`Movie_id`),
  ADD KEY `fk_Character_Series1_idx` (`Series_id`);

--
-- A tábla indexei `movie`
--
ALTER TABLE `movie`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Rating_Character_idx` (`Character_id`),
  ADD KEY `fk_Rating_Movie1_idx` (`Movie_id`),
  ADD KEY `fk_Rating_Series1_idx` (`Series_id`),
  ADD KEY `fk_Rating_User1_idx` (`User_id`);

--
-- A tábla indexei `series`
--
ALTER TABLE `series`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Username_UNIQUE` (`Username`),
  ADD UNIQUE KEY `Email_UNIQUE` (`Email`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `character`
--
ALTER TABLE `character`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT a táblához `movie`
--
ALTER TABLE `movie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT a táblához `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT a táblához `series`
--
ALTER TABLE `series`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT a táblához `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `character`
--
ALTER TABLE `character`
  ADD CONSTRAINT `fk_Character_Movie1` FOREIGN KEY (`Movie_id`) REFERENCES `movie` (`id`),
  ADD CONSTRAINT `fk_Character_Series1` FOREIGN KEY (`Series_id`) REFERENCES `series` (`id`);

--
-- Megkötések a táblához `rating`
--
ALTER TABLE `rating`
  ADD CONSTRAINT `fk_Rating_Character` FOREIGN KEY (`Character_id`) REFERENCES `character` (`id`),
  ADD CONSTRAINT `fk_Rating_Movie1` FOREIGN KEY (`Movie_id`) REFERENCES `movie` (`id`),
  ADD CONSTRAINT `fk_Rating_Series1` FOREIGN KEY (`Series_id`) REFERENCES `series` (`id`),
  ADD CONSTRAINT `fk_Rating_User1` FOREIGN KEY (`User_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
