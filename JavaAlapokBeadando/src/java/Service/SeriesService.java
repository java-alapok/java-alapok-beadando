package Service;

import Model.Series;
import Repository.SeriesRepo;
import java.util.List;
import java.util.Random;

public class SeriesService {
    
    public static String addNewSeries(Series s){
        
        if(s.getTitle().length()>2){
            if(s.getEpisode()>0 && s.getLength()>0){
                if(SeriesRepo.addNewSeries(s)){
                    return "Sikeres rögzítés";
                }
                else{
                    return "Nem sikerült a rögzítés";
                }
            }
            else{
                return "Az epizód hossza vagy az epizódok száma nem megfelelő";
            }
        }
        else{
            return "Nem elég hosszú cím";
        }
    }
    public static String deleteSeries(Series s){
        if(s.getTitle().length()>2){
            if(SeriesRepo.deleteSeries(s)){
                return "Sikeres törlés";
            }
            else{
                return "Sikeretelen törlés";
            }
        }
        else{
            return "Hibás címmegadás";
        }
    }
    
     public static String updateSeries(Series s){
         if(s.getTitle().length()>2){
            if(s.getEpisode()>0 && s.getLength()>0){
                if(SeriesRepo.updateSeries(s)){
                    return "Sikeres módosítás";
                }
                else{
                    return "Nem sikerült a módosítás";
                }
            }
            else{
                return "Az epizód hossza vagy az epizódok száma nem megfelelő";
            }
        }
        else{
            return "Nem elég hosszú cím";
        }
     }
    
     public static List<Series> getallSeries(){
         List<Series> returnList = SeriesRepo.getallSeries();
         return returnList;
     }
     
      public static Series getRecommendedSeries(){
        List<Series> seriesList = SeriesService.getallSeries();
        Series s = new Series(0, null, 0, null, null, null);
        if(!seriesList.isEmpty()){
            Random rand = new Random();
            Integer i = rand.nextInt(seriesList.size());
            s = seriesList.get(i);
        }
        return s;
     }
}
