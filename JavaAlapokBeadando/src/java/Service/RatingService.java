package Service;

import Model.Movie;
import Model.Rating;
import Model.Series;
import Model.User;
import Repository.RatingRepo;
import java.util.List;

public class RatingService {
    
    public static String addNewMovieCharacterRating(Model.Rating r, Movie m, Model.Character c, User u){
        List<Movie> movielist = MovieService.getallMovie();
        Boolean film = Boolean.FALSE;
        for(Movie e : movielist){
            if(e.getTitle().equalsIgnoreCase(m.getTitle())){
                film=Boolean.TRUE;
                break;
            }
        }
        
        List<Model.Character> characterList = CharacterService.getAllCharacter();
        Boolean character = Boolean.FALSE;
        for(Model.Character i :characterList){
            if(i.getName().equalsIgnoreCase(c.getName())){
                character = Boolean.TRUE;
                break;
            }
        }
        User a = UserService.getUserByUsername(u);
        
        if(film && character && a.getId()!=0){
            if(r.getRating()<=10 && r.getRating()>=1){
                if(r.getRating()!=1){
                    if(RatingRepo.addNewMovieCharacterRating(r, m, c, u)){
                        return "Sikeres mentés";
                    }
                    else{
                        return "Nem sikerült a mentés";
                    }
                }
                else{
                    return "Rossz értékelést nem fogadunk el :))";
                }
            }
            else{
                return "A rating értéke 1 és 10 közé kell esssen";
            }
        }
        else{
            return "Hibás adatmegadás: film vagy character vagy user nem létezik";
        }
    }
    
    public static String addNewSeriesCharacterRating(Model.Rating r, Series s, Model.Character c, User u){
        List<Series> seriesList = SeriesService.getallSeries();
        Boolean sorozat = Boolean.FALSE;
        for(Series e : seriesList){
            if(e.getTitle().equalsIgnoreCase(s.getTitle())){
                sorozat=Boolean.TRUE;
                break;
            }
        }
        
        List<Model.Character> characterList = CharacterService.getAllCharacter();
        Boolean character = Boolean.FALSE;
        for(Model.Character i :characterList){
            if(i.getName().equalsIgnoreCase(c.getName())){
                character = Boolean.TRUE;
                break;
            }
        }
        
        User a = UserService.getUserByUsername(u);

        if(sorozat && character && a.getId()!=0){
          if(r.getRating()<=10 && r.getRating()>=1){
                if(r.getRating()!=1){
                    if(RatingRepo.addNewSeriesCharacterRating(r, s, c, u)){
                        return "Sikeres mentés";
                    }
                    else{
                        return "Nem sikerült a mentés";
                    }
                }
                else{
                    return "Rossz értékelést nem fogadunk el :))";
                }
            }
            else{
                return "A rating értéke 1 és 10 közé kell esssen";
            }
        }
        else{
            return "Hibás adatmegadás: Sorozat vagy character vagy User nem létezik";
        }  
    }
    
    public static String addNewMovieRating(Model.Rating r, Movie m, User u){
        List<Movie> movielist = MovieService.getallMovie();
        Boolean film = Boolean.FALSE;
        for(Movie e : movielist){
            if(e.getTitle().equalsIgnoreCase(m.getTitle())){
                film=Boolean.TRUE;
                break;
            }
        }
        User a = UserService.getUserByUsername(u);
        if(film && a.getId()!=0){
            if(r.getRating()<=10 && r.getRating()>=1){
                if(r.getRating()!=1){
                    if(RatingRepo.addNewMovieRating(r, m, u)){
                        return "Sikeres mentés";
                    }
                    else{
                        return "Nem sikerült a mentés";
                    }
                }
                else{
                    return "Rossz értékelést nem fogadunk el :))";
                }
            }
            else{
                return "A rating értéke 1 és 10 közé kell esssen";
            }
        }
        else{
            return "Hibás adatmegadás: film vagy User nem létezik";
        }  
    }
    public static String addNewSeriesRating(Model.Rating r, Series s, User u){
        List<Series> seriesList = SeriesService.getallSeries();
        Boolean sorozat = Boolean.FALSE;
        for(Series e : seriesList){
            if(e.getTitle().equalsIgnoreCase(s.getTitle())){
                sorozat=Boolean.TRUE;
                break;
            }
        }
        User a = UserService.getUserByUsername(u);
        if(sorozat && a.getId()!=0){
            if(r.getRating()<=10 && r.getRating()>=1){
                if(r.getRating()!=1){
                    if(RatingRepo.addNewSeriesRating(r, s, u)){
                        return "Sikeres mentés";
                    }
                    else{
                        return "Nem sikerült a mentés";
                    }
                }
                else{
                    return "Rossz értékelést nem fogadunk el :))";
                }
            }
            else{
                return "A rating értéke 1 és 10 közé kell esssen";
            }
        }
        else{
            return "Hibás adatmegadás: sorozat vagy User nem létezik";
        }  
    }
     public static String deleteRating(Model.Rating r){
        if(r.getId()!=0){
            if(RatingRepo.deleteRating(r)){
                return "Sikeres törlés";
            }
            else{
                return "Sikertelen törlés";
            }
        }
        else{
            return "Hibás adatmegadás";
        }
           
     }
      public static String updateMovieRating(User u, Movie m, Rating r){
        List<Movie> movielist = MovieService.getallMovie();
        Boolean film = Boolean.FALSE;
        for(Movie e : movielist){
            if(e.getTitle().equalsIgnoreCase(m.getTitle())){
                film=Boolean.TRUE;
                break;
            }
        }
        User a = UserService.getUserByUsername(u);
        if(film && a.getId()!=0){
            if(r.getRating()<=10 && r.getRating()>=1){
               if(RatingRepo.updateMovieRating(u, m, r)){
                   return "Sikerres módosítás";
               }
               else{
                   return"Sikertelen módosítás";
               }
            }
            else{
                return "Az értékelésnek 1 és 10 között kell lennie";
            }
        }
        else{
            return "Nincs ilyen film vagy user";
        }
      }
    public static String updateSeriesRating(User u, Series s, Rating r){
        List<Series> seriesList = SeriesService.getallSeries();
        Boolean sorozat = Boolean.FALSE;
        for(Series e : seriesList){
            if(e.getTitle().equalsIgnoreCase(s.getTitle())){
                sorozat=Boolean.TRUE;
                break;
            }
        }
        User a = UserService.getUserByUsername(u);
        if(sorozat && a.getId()!=0){
           if(r.getRating()<=10 && r.getRating()>=1){
               if(RatingRepo.updateSeriesRating(u, s, r)){
                   return "Sikeres módosítás";
               }
               else{
                   return "Sikertelen módosítás";
               }
           }
           else{
               return "Az értékelésnek 1 és 10 között kell lennie";
           }
        }
        else{
           return "Nincs ilyen sorozat vagy user";
        }
    }
    
    public static String updateCharacterRating(User u, Model.Character c, Rating r){
        List<Model.Character> characterList = CharacterService.getAllCharacter();
        Boolean character = Boolean.FALSE;
        for(Model.Character i :characterList){
            if(i.getName().equalsIgnoreCase(c.getName())){
                character = Boolean.TRUE;
                break;
            }
        }
        User a = UserService.getUserByUsername(u);
        if(character && a.getId()!=0){
            if(r.getRating()<=10 && r.getRating()>=1){
                if(RatingRepo.updateCharacterRating(u, c, r)){
                    return "SIkeres módosítás";
                }
                else{
                    return "Sikertelen módosítás";
                }
            }
            else{
                return "Az értékelésnek 1 és 10 között kell lennie";
            }
        }
        else{
            return "A character vagy a user nem található";
        }
    }
    public static Double getAvgRaingByCharacter(Model.Character c){
        List<Integer> ratingList = RatingRepo.getAllRatingByCharacter(c);
        Double avg = 0.0;
        for(Integer i :ratingList){
            avg = avg + i;
        }
        avg = avg / ratingList.size();
        return avg;
    }
    public static Double getAvgRaingByMovie(Movie m){
        List<Integer> ratingList = RatingRepo.getAllRaingByMovie(m);
        Double avg = 0.0;
        for(Integer i :ratingList){
            avg = avg + i;
        }
        avg = avg / ratingList.size();
        return avg;
    }
    public static Double getAvgRaingBySeries(Series s){
        List<Integer> ratingList = RatingRepo.getAllRaingBySeries(s);
        Double avg = 0.0;
        for(Integer i :ratingList){
            avg = avg + i;
        }
        avg = avg / ratingList.size();
        return avg;
    }
    
}
