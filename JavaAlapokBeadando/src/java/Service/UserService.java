package Service;

import Model.User;
import Repository.UserRepo;

public class UserService {
    
   public static String addNewUser(User u){
       User emailValid = UserRepo.getUserByEmail(u);
       User userValid = UserRepo.getUserByUsername(u);
       /*
       Azok a dolgok amik a frontenden lennének validálva:
        - Az email cím az ténylegesen email-e
        - Jelszó1 és Jelszó2 egyezik-e?
       */
       if(emailValid.getId()==0 && userValid.getId()==0){
           if(u.getPassword().length()>8){
               if(UserRepo.addNewUser(u)){
                    return "Sikeres regisztráció";
               }
               else{
                   return "Sikertelen regisztráció";
               }
           }
           else{
               return "A jelszó nem elég hosszú";
           }
       }
       else{
           return "Az email vagy a felhasználónév foglalt";
       }
       
   }
    public static String deleteUser(User u){
       User userValid = UserRepo.getUserByUsername(u);
       
       if(userValid.getId()!=0){
           if(UserRepo.deleteUser(u)){
                return "Sikeres törlés";
           }
           else{
               return "A törlés nem történt meg";
           }
       }
       else{
           return "Nincs ilyen user";
       }
    }
    public static String updateUser(User u){
         User emailvalid = UserRepo.getUserByEmail(u);
         if(emailvalid.getId()==0){
             if(u.getPassword().length()>8){
                 if(UserRepo.updateUser(u)){
                    return "Sikeres adatmódosítás";
                 }
                 else{
                     return "A módosítás nemm történt meg";
                 }
             }
             else{
                 return "A jeszó nem elég hosszú";
             }
         }
         else{
             return "Az email már foglalt";
         }
    }
    
    public static User getUserByUsername(User u){
      User a = UserRepo.getUserByUsername(u);
      return a;
    }
}
