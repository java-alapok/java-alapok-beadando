package Service;

import Model.Movie;
import Model.Series;
import Repository.CharacterRepo;
import java.util.List;

public class CharacterService {
    public static String addNewMovieCharacter(Model.Character c,Movie m){
        List<Movie> movielist = MovieService.getallMovie();
        Boolean film = Boolean.FALSE;
        for(Movie e : movielist){
            if(e.getTitle().equalsIgnoreCase(m.getTitle())){
                film=Boolean.TRUE;
                break;
            }
        }
        if(film){
            if(c.getName().length()>2){
                if(CharacterRepo.addNewMovieCharacter(c, m)){
                    return "Sikeres felvétel";
                }
                else{
                    return "A rögzítés nem sikerült";
                }
            }
            else{
                return "Nem elég hosszú név";
            }
        }
        else{
            return "Nincs ilyen film";
        }
    }
    public static String addNewSeriesCharacter(Model.Character c,Series s){
        List<Series> seriesList = SeriesService.getallSeries();
        Boolean sorozat = Boolean.FALSE;
        for(Series e : seriesList){
            if(e.getTitle().equalsIgnoreCase(s.getTitle())){
                sorozat=Boolean.TRUE;
                break;
            }
        }
        if(sorozat){
            if(c.getName().length()>2){
                if(CharacterRepo.addNewSeriesCharacter(c, s)){
                    return "Sikeres felvétel";
                }
                else{
                    return "A rögzítés nem sikerült";
                }
            }
            else{
                return "Nem elég hosszú név";
            }
        }
        else{
            return "Nincs ilyen sorozat";
        } 
     }
    
    public static String deleteCharacter(Model.Character c){
        List<Model.Character> characterList = CharacterService.getAllCharacter();
        Boolean character = Boolean.FALSE;
        for(Model.Character i :characterList){
            if(i.getName().equalsIgnoreCase(c.getName())){
                character = Boolean.TRUE;
                break;
            }
        }
        if(character){
            if(c.getName().length()>2){
                if(CharacterRepo.deleteCharacter(c)){
                    return "Sikeres törlés";
                }
                else{
                    return "Nem sikerült a törlés";
                }
            }
            else{
                return "Nem megffelelő név";
            }
        }
        else{
            return "Nincs ilyen karakter";
        }
    }
    
    public static List<Model.Character> getAllCharacter(){
         List<Model.Character> returnList = CharacterRepo.getAllCharacter();
         return returnList;
     }
     
    public static String updateSeriesCharacter(Model.Character c, Series s,String name){
        List<Series> seriesList = SeriesService.getallSeries();
        Boolean sorozat = Boolean.FALSE;
        for(Series e : seriesList){
            if(e.getTitle().equalsIgnoreCase(s.getTitle())){
                sorozat=Boolean.TRUE;
                break;
            }
        }
        
        List<Model.Character> characterList = CharacterService.getAllCharacter();
        Boolean character = Boolean.FALSE;
        for(Model.Character i :characterList){
            if(i.getName().equalsIgnoreCase(c.getName())){
                character = Boolean.TRUE;
                break;
            }
        }

        if(sorozat && character){
            if(name.length()>2){
                if(CharacterRepo.updateSeriesCharacter(c, s, name)){
                    return "Sikeres módosítás";
                }
                else {
                    return "Sikertelen módosítás";
                }
            }
            else{
                return "Az új név nem megfelelő";
            }
        }
        else{
            return "A sorozat és vagy a karakter nem található";
        }
     }
    
    public static String updateMovieCharacter(Model.Character c, Movie m,String name){
       List<Movie> movielist = MovieService.getallMovie();
        Boolean film = Boolean.FALSE;
        for(Movie e : movielist){
            if(e.getTitle().equalsIgnoreCase(m.getTitle())){
                film=Boolean.TRUE;
                break;
            }
        }
        
        List<Model.Character> characterList = CharacterService.getAllCharacter();
        Boolean character = Boolean.FALSE;
        for(Model.Character i :characterList){
            if(i.getName().equalsIgnoreCase(c.getName())){
                character = Boolean.TRUE;
                break;
            }
        }
        if(film && character){
            if(name.length()>2){
                if(CharacterRepo.updateMovieCharacter(c, m, name)){
                    return "Sikeres módosítás";
                }
                else {
                    return "Sikertelen módosítás";
                }
            }
            else{
                return "Az új név nem megfelelő";
            }
        }
        else{
            return "A film és vagy a karakter nem található";
        } 
    }
}
