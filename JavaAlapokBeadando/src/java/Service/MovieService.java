
package Service;

import Model.Movie;
import Repository.MovieRepo;
import java.util.List;
import java.util.Random;


public class MovieService {
    public static String addNewMovie(Movie m){
        
        if(m.getTitle().length()>2){
            if(m.getLength()>0){
                if(MovieRepo.addNewMovie(m)){
                    return "Sikeres rögzítés";
                }
                else{
                    return "Nem sikerült a rögzítés";
                }
            }
            else{
                return "A film hossza nem megfelelő";
            }
        }
        else{
            return "Nem elég hosszú cím";
        }
    }
   public static String deleteMovie(Movie m){
        if(m.getTitle().length()>2){
            if(MovieRepo.deleteMovie(m)){
                return "Sikeres törlés";
            }
            else{
                return "Sikeretelen törlés";
            }
        }
        else{
            return "Hibás címmegadás";
        }
    }
    
     public static String updateMovie(Movie m){
         if(m.getTitle().length()>2){
            if(m.getLength()>0){
                if(MovieRepo.updateMovie(m)){
                    return "Sikeres módosítás";
                }
                else{
                    return "Nem sikerült a módosítás";
                }
            }
            else{
                return "A film hossza nem megfelelő";
            }
        }
        else{
            return "Nem elég hosszú cím";
        }
     }
    
      public static List<Movie> getallMovie(){
         List<Movie> returnList = MovieRepo.getallMovie();
         return returnList;
     }
     
     public static Movie getRecommendedMovie(){
        List<Movie> movieList = MovieService.getallMovie();
        Movie m = new Movie(0, null, null, null, null);
        if(!movieList.isEmpty()){
            Random rand = new Random();
            Integer i = rand.nextInt(movieList.size());
            m = movieList.get(i);
        }
        
        return m;
     }
}

