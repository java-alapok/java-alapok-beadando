
package Model;
/**
 *
 * @author Bjakushiki
 */

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Database {
    public static EntityManager getdbConn(){        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("JavaAlapokBeadandoPU");
        EntityManager em = emf.createEntityManager();
        return em;
    }   
}
