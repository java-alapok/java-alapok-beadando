/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Benedek
 */
@Entity
@Table(name = "series")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Series.findAll", query = "SELECT s FROM Series s")
    , @NamedQuery(name = "Series.findById", query = "SELECT s FROM Series s WHERE s.id = :id")
    , @NamedQuery(name = "Series.findByTitle", query = "SELECT s FROM Series s WHERE s.title = :title")
    , @NamedQuery(name = "Series.findByEpisode", query = "SELECT s FROM Series s WHERE s.episode = :episode")
    , @NamedQuery(name = "Series.findByLength", query = "SELECT s FROM Series s WHERE s.length = :length")
    , @NamedQuery(name = "Series.findByGenre", query = "SELECT s FROM Series s WHERE s.genre = :genre")
    , @NamedQuery(name = "Series.findByIsActive", query = "SELECT s FROM Series s WHERE s.isActive = :isActive")})
public class Series implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Title")
    private String title;
    @Basic(optional = false)
    @Column(name = "Episode")
    private int episode;
    @Basic(optional = false)
    @Column(name = "Length")
    private Integer length;
    @Basic(optional = false)
    @Column(name = "Genre")
    private String genre;
    @Basic(optional = false)
    @Column(name = "IsActive")
    private Integer isActive;
    @OneToMany(mappedBy = "seriesid")
    private Collection<Character> characterCollection;
    @OneToMany(mappedBy = "seriesid")
    private Collection<Rating> ratingCollection;

    public Series() {
    }

    public Series(Integer id) {
        this.id = id;
    }

    public Series(Integer id, String title, int episode, Integer length, String genre, Integer isActive) {
        this.id = id;
        this.title = title;
        this.episode = episode;
        this.length = length;
        this.genre = genre;
        this.isActive = isActive;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getEpisode() {
        return episode;
    }

    public void setEpisode(int episode) {
        this.episode = episode;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    @XmlTransient
    public Collection<Character> getCharacterCollection() {
        return characterCollection;
    }

    public void setCharacterCollection(Collection<Character> characterCollection) {
        this.characterCollection = characterCollection;
    }

    @XmlTransient
    public Collection<Rating> getRatingCollection() {
        return ratingCollection;
    }

    public void setRatingCollection(Collection<Rating> ratingCollection) {
        this.ratingCollection = ratingCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Series)) {
            return false;
        }
        Series other = (Series) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Series[ id=" + id + " ]";
    }
    
}
