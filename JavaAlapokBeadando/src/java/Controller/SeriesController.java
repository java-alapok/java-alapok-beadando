
package Controller;

import Model.Series;
import Service.SeriesService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Benedek
 */
public class SeriesController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            if(request.getParameter("task").equals("addNewSeries")){
                JSONObject valasz = new JSONObject();
            
                if(request.getParameter("title").isEmpty() || request.getParameter("length").isEmpty() ||
                        request.getParameter("genre").isEmpty() || request.getParameter("episode").isEmpty()){
                    valasz.put("result", "A mezők nincsenek megfelelően kitöltve");
                }
                else{
                    String title = request.getParameter("title");
                    Integer episode = Integer.parseInt(request.getParameter("episode"));
                    Integer length = Integer.parseInt(request.getParameter("length"));
                    String genre = request.getParameter("genre");
                    
                    Series s = new Series(null, title, episode, length, genre, null);
                    String serviceResult = SeriesService.addNewSeries(s);
                   
                    valasz.put("result", serviceResult);   
                }
                out.println(valasz.toString());
            }
            if(request.getParameter("task").equals("updateSeries")){
                JSONObject valasz = new JSONObject();
            
                if(request.getParameter("title").isEmpty() || request.getParameter("length").isEmpty() ||
                        request.getParameter("genre").isEmpty() || request.getParameter("id").isEmpty()){
                    valasz.put("result", "A mezők nincsenek megfelelően kitöltve");
                }
                else{
                    String title = request.getParameter("title");
                    Integer episode = Integer.parseInt(request.getParameter("episode"));
                    Integer length = Integer.parseInt(request.getParameter("length"));
                    String genre = request.getParameter("genre");
                    Integer id = Integer.parseInt(request.getParameter("id"));
                    
                    Series s = new Series(id, title, episode, length, genre, null);
                    String serviceResult = SeriesService.updateSeries(s);
                    
                    valasz.put("result", serviceResult);   
                }
                out.println(valasz.toString());
            }
            if(request.getParameter("task").equals("deleteSeries")){
                JSONObject valasz = new JSONObject();
                if(request.getParameter("title").isEmpty()){
                   valasz.put("result", "A mező nincs megfelelően kitöltve"); 
                }
                else{
                    String title = request.getParameter("title");
                    Series s = new Series(null, title, 0, null, null, null);
                    String serviceResult = SeriesService.deleteSeries(s);
                    
                    valasz.put("result", serviceResult);
                }
               out.println(valasz.toString());    
            }
             if(request.getParameter("task").equals("getallSeries")){
                List<Series> allSeries = SeriesService.getallSeries();
                JSONArray valasz = new JSONArray();
                if(allSeries.isEmpty()){
                    JSONObject exObj = new JSONObject();
                    exObj.put("result","Nem található sorozat");
                    valasz.put(exObj);
                }
                try{
                for(Series s: allSeries){
                    JSONObject exObj = new JSONObject();
                    exObj.put("id", s.getId());
                    exObj.put("Title", s.getTitle());
                    exObj.put("Episode", s.getEpisode());
                    exObj.put("Length", s.getLength());
                    exObj.put("Genre", s.getGenre());
                    
                    valasz.put(exObj);
                    }
                }
                catch(Exception ex){
                        System.out.println(ex);
                        }
                out.write(valasz.toString());
        }
             if(request.getParameter("task").equals("getRecommendedSeries")){
                JSONObject valasz = new JSONObject();
                String resultString="";
                
                Series s = SeriesService.getRecommendedSeries();
                if(s.getId()!=0){
                    resultString="A neked ajánlott Sorozat: "+s.getTitle();
                }
                else{
                    resultString="Hiba, nem található film";
                }
                valasz.put("result", resultString);
                
               out.println(valasz.toString());    
            }
            
            
            
            
            
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
