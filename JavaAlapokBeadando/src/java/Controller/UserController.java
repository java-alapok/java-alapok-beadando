/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.User;
import Service.UserService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Bjakushiki
 */
public class UserController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            if(request.getParameter("task").equals("addNewUser")){
                JSONObject valasz = new JSONObject();
            
                if(request.getParameter("email").isEmpty() || request.getParameter("password").isEmpty() ||
                        request.getParameter("name").isEmpty()){
                    valasz.put("result", "A mezők nincsenek megfelelően kitöltve");
                }
                else{
                    String email = request.getParameter("email");
                    String pw = request.getParameter("password");
                    String name = request.getParameter("name");
                    
                    User u = new User(null, name, pw, email, null);
                    
                    String serviceResult = UserService.addNewUser(u);
                    
                    valasz.put("result", serviceResult);   
                }
                out.println(valasz.toString());
            } 

            if(request.getParameter("task").equals("updateUser")){
               JSONObject valasz = new JSONObject();
               if(request.getParameter("email").isEmpty() || 
                  request.getParameter("password").isEmpty() ||
                  request.getParameter("name").isEmpty()){
                   
                    valasz.put("result", "A mezők nincsenek megfelelően kitöltve");
                }
                else{
                    String email = request.getParameter("email");
                    String pw = request.getParameter("password");
                    String name = request.getParameter("name");
                    
                    User u = new User(null, name, pw, email, null);
                    
                    String serviceResult = UserService.updateUser(u);
                    
                    valasz.put("result", serviceResult);
                   
               } 
                 
               out.println(valasz.toString());   
             }
            if(request.getParameter("task").equals("deleteUser")){
                JSONObject valasz = new JSONObject();
                if(request.getParameter("name").isEmpty()){
                   valasz.put("result", "A mező nincs megfelelően kitöltve"); 
                }
                else{
                    String name = request.getParameter("name");
                    User u = new User(null, name, null, null, null);
                    String serviceResult = UserService.deleteUser(u);
                    
                    valasz.put("result", serviceResult);
                }
               out.println(valasz.toString());    
            }
             
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
