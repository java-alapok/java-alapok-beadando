
package Controller;

import Model.Character;
import Model.Movie;
import Model.Rating;
import Model.Series;
import Model.User;
import Service.RatingService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Benedek
 */
public class RatingController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
             if(request.getParameter("task").equals("addNewMovieCharacterRating")){
                JSONObject valasz = new JSONObject();
            
                if(request.getParameter("username").isEmpty() || 
                        request.getParameter("character").isEmpty()||
                        request.getParameter("movie").isEmpty()||
                        request.getParameter("rating").isEmpty()){
                    valasz.put("result", "A mezők nincsenek megfelelően kitöltve");
                }
                else{
                    String username = request.getParameter("username");
                    String character = request.getParameter("character");
                    String movie = request.getParameter("movie");
                    Integer rating = Integer.parseInt(request.getParameter("rating"));
                    
                    User u = new User(null, username, null, null, null);
                    Character c = new Character(null, character, null);
                    Movie m = new Movie(null, movie, null, null, null);
                    Rating r = new Rating(null, rating, null);
                    
                    String serviceResult = RatingService.addNewMovieCharacterRating(r, m, c, u);
                    
                    valasz.put("result", serviceResult);   
                }
                out.println(valasz.toString());
            }
             
            if(request.getParameter("task").equals("addNewSeriesCharacterRating")){
                JSONObject valasz = new JSONObject();
            
                if(request.getParameter("username").isEmpty() || 
                        request.getParameter("character").isEmpty()||
                        request.getParameter("series").isEmpty()||
                        request.getParameter("rating").isEmpty()){
                    valasz.put("result", "A mezők nincsenek megfelelően kitöltve");
                }
                else{
                    String username = request.getParameter("username");
                    String character = request.getParameter("character");
                    String series = request.getParameter("series");
                    Integer rating = Integer.parseInt(request.getParameter("rating"));
                    
                    User u = new User(null, username, null, null, null);
                    Character c = new Character(null, character, null);
                    Series s = new Series(null, series, 0, null, null, null);
                    Rating r = new Rating(null, rating, null);
                    
                    String serviceResult = RatingService.addNewSeriesCharacterRating(r, s, c, u);
                    
                    valasz.put("result", serviceResult);   
                }
                out.println(valasz.toString());
            }
            
            if(request.getParameter("task").equals("addNewSeriesRating")){
                JSONObject valasz = new JSONObject();
            
                if(request.getParameter("username").isEmpty() || 
                        request.getParameter("series").isEmpty()||
                        request.getParameter("rating").isEmpty()){
                    valasz.put("result", "A mezők nincsenek megfelelően kitöltve");
                }
                else{
                    String username = request.getParameter("username");
                    
                    String series = request.getParameter("series");
                    Integer rating = Integer.parseInt(request.getParameter("rating"));
                    
                    User u = new User(null, username, null, null, null);
                    
                    Series s = new Series(null, series, 0, null, null, null);
                    Rating r = new Rating(null, rating, null);
                    
                    String serviceResult = RatingService.addNewSeriesRating(r, s, u);
                    
                    valasz.put("result", serviceResult);   
                }
                out.println(valasz.toString());
            }
            if(request.getParameter("task").equals("addNewMovieRating")){
                JSONObject valasz = new JSONObject();
            
                if(request.getParameter("username").isEmpty() || 
                        request.getParameter("movie").isEmpty()||
                        request.getParameter("rating").isEmpty()){
                    valasz.put("result", "A mezők nincsenek megfelelően kitöltve");
                }
                else{
                    String username = request.getParameter("username");
                   
                    String movie = request.getParameter("movie");
                    Integer rating = Integer.parseInt(request.getParameter("rating"));
                    
                    User u = new User(null, username, null, null, null);
                   
                    Movie m = new Movie(null, movie, null, null, null);
                    Rating r = new Rating(null, rating, null);
                    
                    String serviceResult = RatingService.addNewMovieRating(r, m, u);
                    
                    valasz.put("result", serviceResult);   
                }
                out.println(valasz.toString());
            }
            
            if(request.getParameter("task").equals("deleteRating")){
                JSONObject valasz = new JSONObject();
                if(request.getParameter("id").isEmpty()){
                   valasz.put("result", "A mező nincs megfelelően kitöltve"); 
                }
                else{
                    Integer id = Integer.parseInt(request.getParameter("id"));
                    Rating r = new Rating(id, 0, null);
                    String serviceResult = RatingService.deleteRating(r);
                    
                    valasz.put("result", serviceResult);
                }
               out.println(valasz.toString());    
            }
            if(request.getParameter("task").equals("updateMovieRating")){
                JSONObject valasz = new JSONObject();
            
                if(request.getParameter("username").isEmpty() || 
                        request.getParameter("rating").isEmpty() ||
                        request.getParameter("title").isEmpty()){
                    valasz.put("result", "A mezők nincsenek megfelelően kitöltve");
                }
                else{
                    String title = request.getParameter("title");
                    Integer rating = Integer.parseInt(request.getParameter("rating"));
                    String username = request.getParameter("username");
                    
                    
                    User u  = new User(null, username, null, null, null);
                    Movie m = new Movie(null, title, null, null, null);
                    Rating r = new Rating(null,rating,null);
                    String serviceResult = RatingService.updateMovieRating(u, m, r);
                    
                    valasz.put("result", serviceResult);   
                }
                out.println(valasz.toString());
            }
            if(request.getParameter("task").equals("updateSeriesRating")){
                JSONObject valasz = new JSONObject();
            
                if(request.getParameter("username").isEmpty() || 
                        request.getParameter("rating").isEmpty() ||
                        request.getParameter("title").isEmpty()){
                    valasz.put("result", "A mezők nincsenek megfelelően kitöltve");
                }
                else{
                    String title = request.getParameter("title");
                    Integer rating = Integer.parseInt(request.getParameter("rating"));
                    String username = request.getParameter("username");
                    
                    
                    User u  = new User(null, username, null, null, null);
                    Series s = new Series(null, title, 0, null, null, null);
                    Rating r = new Rating(null,rating,null);
                    String serviceResult = RatingService.updateSeriesRating(u, s, r);
                    
                    valasz.put("result", serviceResult);   
                }
                out.println(valasz.toString());
            }
            if(request.getParameter("task").equals("updateCharacterRating")){
                JSONObject valasz = new JSONObject();
            
                if(request.getParameter("username").isEmpty() || 
                        request.getParameter("rating").isEmpty() ||
                        request.getParameter("name").isEmpty()){
                    valasz.put("result", "A mezők nincsenek megfelelően kitöltve");
                }
                else{
                    String name = request.getParameter("name");
                    Integer rating = Integer.parseInt(request.getParameter("rating"));
                    String username = request.getParameter("username");
                    
                    
                    User u  = new User(null, username, null, null, null);
                    Character c = new Character(null, name, null);
                    Rating r = new Rating(null,rating,null);
                    String serviceResult = RatingService.updateCharacterRating(u, c, r);
                    
                    valasz.put("result", serviceResult);   
                }
                out.println(valasz.toString());
            }
            if(request.getParameter("task").equals("getAvgRaingByCharacter")){
                JSONObject valasz = new JSONObject();
               if(request.getParameter("name").isEmpty()){
                   valasz.put("result", "A mezők nincsenek megfelelően kitöltve");
                   
               }
               else{
                   String name = request.getParameter("name");
                   Character c = new Character(null, name, null);
                   String valaszString="";
                   Double serviceResult = RatingService.getAvgRaingByCharacter(c);
                   if(!Double.isNaN(serviceResult)){
                        valaszString = "A(z) "+c.getName()+" Karakter átlagos értékelése: "+serviceResult;
                   }
                   else{
                        valaszString = "A megadott Karakterhez nem tartozik értékelés";   
                   }
                   
                   valasz.put("result", valaszString);
               }
               out.println(valasz.toString());
            }
            
            if(request.getParameter("task").equals("getAvgRaingByMovie")){
                JSONObject valasz = new JSONObject();
               if(request.getParameter("title").isEmpty()){
                   valasz.put("result", "A mezők nincsenek megfelelően kitöltve");
                   
               }
               else{
                   String title = request.getParameter("title");
                   Movie m = new Movie(null, title, null, null, null);
                   String valaszString="";
                   Double serviceResult = RatingService.getAvgRaingByMovie(m);
                   if(!Double.isNaN(serviceResult)){
                    valaszString = "A(z) "+m.getTitle()+" Film átlagos értékelése: "+serviceResult;
                   }
                   else{
                        valaszString = "A megadott Karakterhez nem tartozik értékelés";   
                   }
                   
                   valasz.put("result", valaszString);
               }
               out.println(valasz.toString());
            }
            if(request.getParameter("task").equals("getAvgRaingBySeries")){
                JSONObject valasz = new JSONObject();
               if(request.getParameter("title").isEmpty()){
                   valasz.put("result", "A mezők nincsenek megfelelően kitöltve");
                   
               }
               else{
                   String title = request.getParameter("title");
                   Series s = new Series(null, title, 0, null, null, null);
                   String valaszString="";
                   Double serviceResult = RatingService.getAvgRaingBySeries(s);
                   if(!Double.isNaN(serviceResult)){
                      valaszString = "A(z) "+s.getTitle()+" Sorozat átlagos értékelése: "+serviceResult;  
                   }
                   else{
                        valaszString = "A megadott sorozathoz nem tartozik értékelés";   
                   }
                   
                   
                   valasz.put("result", valaszString);
               }
               out.println(valasz.toString());
            }
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
