
package Controller;

import Model.Movie;
import Service.MovieService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Benedek
 */
public class MovieController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            if(request.getParameter("task").equals("addNewMovie")){
                JSONObject valasz = new JSONObject();
            
                if(request.getParameter("title").isEmpty() || request.getParameter("length").isEmpty() ||
                        request.getParameter("genre").isEmpty()){
                    valasz.put("result", "A mezők nincsenek megfelelően kitöltve");
                }
                else{
                    String title = request.getParameter("title");
                    Integer length = Integer.parseInt(request.getParameter("length"));
                    String genre = request.getParameter("genre");
                    
                    Movie m = new Movie(null, title, length, genre, null);
                    String serviceResult = MovieService.addNewMovie(m);
                    
                    valasz.put("result", serviceResult);   
                }
                out.println(valasz.toString());
            }
            
            if(request.getParameter("task").equals("updateMovie")){
                JSONObject valasz = new JSONObject();
            
                if(request.getParameter("title").isEmpty() || request.getParameter("length").isEmpty() ||
                        request.getParameter("genre").isEmpty() || request.getParameter("id").isEmpty()){
                    valasz.put("result", "A mezők nincsenek megfelelően kitöltve");
                }
                else{
                    String title = request.getParameter("title");
                    Integer length = Integer.parseInt(request.getParameter("length"));
                    String genre = request.getParameter("genre");
                    Integer id = Integer.parseInt(request.getParameter("id"));
                    
                    Movie m = new Movie(id, title, length, genre, null);
                    String serviceResult = MovieService.updateMovie(m);
                    
                    valasz.put("result", serviceResult);   
                }
                out.println(valasz.toString());
            }
            if(request.getParameter("task").equals("deleteMovie")){
                JSONObject valasz = new JSONObject();
                if(request.getParameter("title").isEmpty()){
                   valasz.put("result", "A mező nincs megfelelően kitöltve"); 
                }
                else{
                    String title = request.getParameter("title");
                    Movie m = new Movie(null, title, null, null, null);
                    String serviceResult = MovieService.deleteMovie(m);
                    
                    valasz.put("result", serviceResult);
                }
               out.println(valasz.toString());    
            }
             if(request.getParameter("task").equals("getallMovie")){
                List<Movie> allMovie = MovieService.getallMovie();
                JSONArray valasz = new JSONArray();
                if(allMovie.isEmpty()){
                    JSONObject exObj = new JSONObject();
                    exObj.put("result","Nem található film");
                    valasz.put(exObj);
                }
                try{
                for(Movie m: allMovie){
                    JSONObject exObj = new JSONObject();
                    exObj.put("id", m.getId());
                    exObj.put("Title", m.getTitle());
                    exObj.put("Length", m.getLength());
                    exObj.put("Genre", m.getGenre());
                    
                    valasz.put(exObj);
                    }
                }
                catch(Exception ex){
                        System.out.println(ex);
                        
                        }
                out.write(valasz.toString());
        }
             if(request.getParameter("task").equals("getRecommendedMovie")){
                JSONObject valasz = new JSONObject();
                String resultString="";
                Movie m = MovieService.getRecommendedMovie();
                if(m.getId()!=0){
                   resultString="A neked ajánlott film: "+m.getTitle(); 
                }
                else{
                    resultString="Hiba, nem található film";
                }
                

                valasz.put("result", resultString);
                
               out.println(valasz.toString());    
            }
           
            
            
            
            
            
            
            
            
            
            
            
            
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
