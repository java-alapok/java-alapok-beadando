
package Controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import Model.Character;
import Model.Movie;
import Model.Series;
import Service.CharacterService;
import java.util.List;
import org.json.JSONArray;

/**
 *
 * @author Benedek
 */
public class CharacterController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
           if(request.getParameter("task").equals("addNewMovieCharacter")){
                JSONObject valasz = new JSONObject();
            
                if(request.getParameter("name").isEmpty() || 
                        request.getParameter("movietitle").isEmpty()){
                    valasz.put("result", "A mezők nincsenek megfelelően kitöltve");
                }
                else{
                    String name = request.getParameter("name");
                    String movietitle = request.getParameter("movietitle");
                    Character c = new Character(null, name, null);
                    Movie m = new Movie(null, movietitle, null, null, null);
                    
                    String serviceResult = CharacterService.addNewMovieCharacter(c, m);
                    
                    valasz.put("result", serviceResult);   
                }
                out.println(valasz.toString());
            }
           if(request.getParameter("task").equals("addNewSeriesCharacter")){
                JSONObject valasz = new JSONObject();
            
                if(request.getParameter("name").isEmpty() || 
                        request.getParameter("seriestitle").isEmpty()){
                    valasz.put("result", "A mezők nincsenek megfelelően kitöltve");
                }
                else{
                    String name = request.getParameter("name");
                    String seriestitle = request.getParameter("seriestitle");
                    Character c = new Character(null, name, null);
                    Series s = new Series(null, seriestitle, 0, null, null, null);
                    
                    String serviceResult = CharacterService.addNewSeriesCharacter(c, s);
                    
                    valasz.put("result", serviceResult);   
                }
                out.println(valasz.toString());
            }
           if(request.getParameter("task").equals("updateMovieCharacter")){
                JSONObject valasz = new JSONObject();
            
                if(request.getParameter("name").isEmpty() || 
                        request.getParameter("movietitle").isEmpty() || 
                        request.getParameter("newname").isEmpty()){
                    valasz.put("result", "A mezők nincsenek megfelelően kitöltve");
                }
                else{
                    String name = request.getParameter("name");
                    String movietitle = request.getParameter("movietitle");
                    String newname = request.getParameter("newname");
                    Character c = new Character(null, name, null);
                    Movie m = new Movie(null, movietitle, null, null, null);
                    
                    String serviceResult = CharacterService.updateMovieCharacter(c, m,newname);
                    
                    valasz.put("result", serviceResult);   
                }
                out.println(valasz.toString());
            }
           if(request.getParameter("task").equals("updateSeriesCharacter")){
                JSONObject valasz = new JSONObject();
            
                if(request.getParameter("name").isEmpty() || 
                        request.getParameter("seriestitle").isEmpty() ||
                        request.getParameter("newname").isEmpty()){
                    valasz.put("result", "A mezők nincsenek megfelelően kitöltve");
                }
                else{
                    String name = request.getParameter("name");
                    String seriestitle = request.getParameter("seriestitle");
                    String newname = request.getParameter("newname");
                    Character c = new Character(null, name, null);
                    Series s = new Series(null, seriestitle, 0, null, null, null);
                    
                    String serviceResult = CharacterService.updateSeriesCharacter(c, s, newname);
                    
                    valasz.put("result", serviceResult);   
                }
                out.println(valasz.toString());
            }
           if(request.getParameter("task").equals("deleteCharacter")){
                JSONObject valasz = new JSONObject();
                if(request.getParameter("name").isEmpty()){
                   valasz.put("result", "A mező nincs megfelelően kitöltve"); 
                }
                else{
                    String name = request.getParameter("name");
                    Character c = new Character(null, name, null);
                    String serviceResult = CharacterService.deleteCharacter(c);
                    valasz.put("result", serviceResult);
                }
               out.println(valasz.toString());    
            }
           if(request.getParameter("task").equals("getAllCharacter")){
                List<Character> allCharacter = CharacterService.getAllCharacter();
                JSONArray valasz = new JSONArray();
                if(allCharacter.isEmpty()){
                    JSONObject exObj = new JSONObject();
                    exObj.put("result","Nem található karakter");
                    valasz.put(exObj);
                }
                try{
                for(Character c: allCharacter){
                    JSONObject exObj = new JSONObject();
                    exObj.put("id", c.getId());
                    exObj.put("Name", c.getName());

                    valasz.put(exObj);
                    }
                }
                catch(Exception ex){
                        System.out.println(ex);
                        }
                out.write(valasz.toString());
        }
           
           
           
           
           
           
           
           
           
           
           
           
           
           
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
