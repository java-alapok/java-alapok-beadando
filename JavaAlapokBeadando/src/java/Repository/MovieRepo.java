
package Repository;

import Model.Database;
import Model.Movie;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author T omi
 */
public class MovieRepo {
        public static Boolean addNewMovie(Movie m){
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("create_movie");
           
            spq.registerStoredProcedureParameter("titleIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("lengthIN", Integer.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("genreIN", String.class, ParameterMode.IN);
            
            spq.setParameter("titleIN", m.getTitle());
            spq.setParameter("lengthIN", m.getLength());
            spq.setParameter("genreIN", m.getGenre());
            
            
            spq.execute();
            return Boolean.TRUE;
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }
        
    }
        public static Boolean deleteMovie(Movie m){
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("delete_movie_by_title");
           
            spq.registerStoredProcedureParameter("nameIN", String.class, ParameterMode.IN);
            
            spq.setParameter("nameIN", m.getTitle());
            
            
            spq.execute();
            return Boolean.TRUE;
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }
        
    }
        public static List<Movie> getallMovie(){
            List<Movie> returnList = new ArrayList<>();
            Movie returnMovie = new Movie(0,"Nincs adat",0,"Nincs adat",1);
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("get_all_movie_title");
           
            List<Object[]> objectList = spq.getResultList();
            for(Object [] object : objectList){
                Movie m = new Movie();
                m.setId(Integer.valueOf(String.valueOf(object[0])));
                m.setTitle(String.valueOf(object[1]));
                m.setLength(Integer.valueOf(String.valueOf(object[2])));
                m.setGenre(String.valueOf(object[3]));
                m.setIsActive(1);
                
                returnList.add(m);
                
            }

            return returnList;
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return returnList;
        }
        
    }
        public static Boolean updateMovie(Movie m){
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("update_movie");
            
            spq.registerStoredProcedureParameter("idIN", Integer.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("titleIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("lengthIN", Integer.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("genreIN", String.class, ParameterMode.IN);
            
            spq.setParameter("idIN", m.getId());
            spq.setParameter("titleIN", m.getTitle());
            spq.setParameter("lengthIN", m.getLength());
            spq.setParameter("genreIN", m.getGenre());
            
            
            spq.execute();
            return Boolean.TRUE;
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }
        
    }
}
