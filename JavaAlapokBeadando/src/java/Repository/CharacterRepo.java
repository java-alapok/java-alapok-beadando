
package Repository;

import Model.Database;
import Model.Movie;
import Model.Series;
import Model.Character;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author T omi
 */
public class CharacterRepo {

    public static Boolean addNewMovieCharacter(Character c,Movie m){
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("create_movie_character");
           
            spq.registerStoredProcedureParameter("nameIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("movienameIN", String.class, ParameterMode.IN);
            
            spq.setParameter("nameIN", c.getName());
            spq.setParameter("movienameIN", m.getTitle());
            
            
            spq.execute();
            return Boolean.TRUE;
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }
        
    }
    public static Boolean addNewSeriesCharacter(Character c,Series s){
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("create_series_character");
           
            spq.registerStoredProcedureParameter("nameIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("seriesnameIN", String.class, ParameterMode.IN);
            
            spq.setParameter("nameIN", c.getName());
            spq.setParameter("seriesnameIN", s.getTitle());
            
            
            spq.execute();
            return Boolean.TRUE;
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }
        
    }
    public static Boolean deleteCharacter(Character c){
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("delete_character_by_name");
           
            spq.registerStoredProcedureParameter("nameIN", String.class, ParameterMode.IN);
            
            spq.setParameter("nameIN", c.getName());
            
            
            spq.execute();
            return Boolean.TRUE;
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }
        
    }
     public static List<Character> getAllCharacter(){
            List<Character> returnList = new ArrayList<>();
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("get_all_character");
           
            List<Object[]> objectList = spq.getResultList();
            for(Object [] object : objectList){
                Character c = new Character();
                c.setId(Integer.valueOf(String.valueOf(object[0])));
                c.setName(String.valueOf(object[1]));
                c.setIsActive(1);
                
                returnList.add(c);
            }

            return returnList;
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return returnList;
        }
    }
    public static Boolean updateSeriesCharacter(Model.Character c, Series s,String name){
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("update_series_character_by_name");

            spq.registerStoredProcedureParameter("nameIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("seriesIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("newnameIN", String.class, ParameterMode.IN);

            spq.setParameter("nameIN", c.getName());
            spq.setParameter("seriesIN", s.getTitle());
            spq.setParameter("newnameIN", name);


            spq.execute();
            return Boolean.TRUE;

        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }

    }
    public static Boolean updateMovieCharacter(Model.Character c, Movie m,String name){
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("update_movie_character_by_name");

            spq.registerStoredProcedureParameter("nameIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("movieIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("newnameIN", String.class, ParameterMode.IN);

            spq.setParameter("nameIN", c.getName());
            spq.setParameter("movieIN", m.getTitle());
            spq.setParameter("newnameIN", name);


            spq.execute();
            return Boolean.TRUE;

        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }

    }
}
