
package Repository;

import Model.Database;
import Model.Series;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author T omi
 */
public class SeriesRepo {
    public static Boolean addNewSeries(Series s){
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("create_series");
           
            spq.registerStoredProcedureParameter("titleIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("episodeIN", Integer.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("lengthIN", Integer.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("genreIN", String.class, ParameterMode.IN);
            
            spq.setParameter("titleIN", s.getTitle());
            spq.setParameter("episodeIN", s.getEpisode());
            spq.setParameter("lengthIN", s.getLength());
            spq.setParameter("genreIN", s.getGenre());
            
            
            spq.execute();
            return Boolean.TRUE;
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }
        
    }
    public static Boolean deleteSeries(Series s){
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("delete_series_by_title");
           
            spq.registerStoredProcedureParameter("nameIN", String.class, ParameterMode.IN);
            
            spq.setParameter("nameIN", s.getTitle());
            
            
            spq.execute();
            return Boolean.TRUE;
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }
        
    }
    public static List<Series> getallSeries(){
            List<Series> returnList = new ArrayList<>();
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("get_all_series_title");
           
            List<Object[]> objectList = spq.getResultList();
            for(Object [] object : objectList){
                Series s = new Series();
                s.setId(Integer.valueOf(String.valueOf(object[0])));
                s.setTitle(String.valueOf(object[1]));
                s.setEpisode(Integer.valueOf(String.valueOf(object[2])));
                s.setLength(Integer.valueOf(String.valueOf(object[3])));
                s.setGenre(String.valueOf(object[4]));
                s.setIsActive(1);
                
                returnList.add(s);
            }

            return returnList;
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return returnList;
        }
        
    }
    public static Boolean updateSeries(Series s){
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("update_series");
            
            spq.registerStoredProcedureParameter("idIN", Integer.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("titleIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("episodeIN", Integer.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("lengthIN", Integer.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("genreIN", String.class, ParameterMode.IN);
            
            spq.setParameter("idIN", s.getId());
            spq.setParameter("titleIN", s.getTitle());
            spq.setParameter("episodeIN", s.getEpisode());
            spq.setParameter("lengthIN", s.getLength());
            spq.setParameter("genreIN", s.getGenre());
            
            
            spq.execute();
            return Boolean.TRUE;
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }
        
    }
}
