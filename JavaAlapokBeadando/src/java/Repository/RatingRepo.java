
package Repository;

import Model.Database;
import Model.Movie;
import Model.Series;
import Model.Rating;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import Model.User;
import com.sun.xml.internal.ws.policy.sourcemodel.ModelNode;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author T omi
 */
public class RatingRepo {
    //semmi nincs megcsinálva 
    public static Boolean addNewMovieCharacterRating(Model.Rating r, Movie m, Model.Character c, User u){
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("create_movie_character_rating");
           
            spq.registerStoredProcedureParameter("usernameIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("charcternameIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("movienameIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("ratingIN", Integer.class, ParameterMode.IN);
            
            spq.setParameter("usernameIN", u.getUsername());
            spq.setParameter("charcternameIN", c.getName());
            spq.setParameter("movienameIN", m.getTitle());
            spq.setParameter("ratingIN", r.getRating());
            
            spq.execute();
            return Boolean.TRUE;
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }
        
    }
     public static Boolean addNewSeriesCharacterRating(Model.Rating r, Series s, Model.Character c, User u){
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("create_series_character_rating");
           
            spq.registerStoredProcedureParameter("usernameIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("charcternameIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("movienameIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("ratingIN", Integer.class, ParameterMode.IN);
            
            spq.setParameter("usernameIN", u.getUsername());
            spq.setParameter("charcternameIN", c.getName());
            spq.setParameter("movienameIN", s.getTitle());
            spq.setParameter("ratingIN", r.getRating());
            
            spq.execute();
            return Boolean.TRUE;
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }
        
    }
    public static Boolean addNewSeriesRating(Model.Rating r, Series s, User u){
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("create_series_rating");
           
            spq.registerStoredProcedureParameter("usernameIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("seriesnameIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("ratingIN", Integer.class, ParameterMode.IN);
            
            spq.setParameter("usernameIN", u.getUsername());
            spq.setParameter("seriesnameIN", s.getTitle());
            spq.setParameter("ratingIN", r.getRating());
            
            spq.execute();
            return Boolean.TRUE;
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }
        
    }
    public static Boolean addNewMovieRating(Model.Rating r, Movie m, User u){
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("create_movie_rating");
           
            spq.registerStoredProcedureParameter("usernameIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("movienameIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("ratingIN", Integer.class, ParameterMode.IN);
            
            spq.setParameter("usernameIN", u.getUsername());
            spq.setParameter("movienameIN", m.getTitle());
            spq.setParameter("ratingIN", r.getRating());
            
            spq.execute();
            return Boolean.TRUE;
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }
        
    }
     
     
     
    public static Boolean deleteRating(Model.Rating r){
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("delete_rating_by_id");
           
            spq.registerStoredProcedureParameter("idIN", Integer.class, ParameterMode.IN);
            
            spq.setParameter("idIN", r.getId());
            
            spq.execute();
            return Boolean.TRUE;
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }
        
    }
    
    public static Boolean updateMovieRating(User u, Movie m, Rating r){
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("update_movie_rating_by_name_and_user");

            spq.registerStoredProcedureParameter("nameIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("newratingIN", Integer.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("usernameIN", String.class, ParameterMode.IN);

            spq.setParameter("nameIN", m.getTitle());
            spq.setParameter("newratingIN", r.getRating());
            spq.setParameter("usernameIN", u.getUsername());

            spq.execute();
            return Boolean.TRUE;

        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }

    }
    
    public static Boolean updateSeriesRating(User u, Series s, Rating r){
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("update_series_rating_by_name_and_user");

            spq.registerStoredProcedureParameter("nameIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("newratingIN", Integer.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("usernameIN", String.class, ParameterMode.IN);

            spq.setParameter("nameIN", s.getTitle());
            spq.setParameter("newratingIN", r.getRating());
            spq.setParameter("usernameIN", u.getUsername());

            spq.execute();
            return Boolean.TRUE;

        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }

}
    public static Boolean updateCharacterRating(User u, Model.Character c, Rating r){
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("update_character_rating_by_name_and_user");

            spq.registerStoredProcedureParameter("nameIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("newratingIN", Integer.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("usernameIN", String.class, ParameterMode.IN);

            spq.setParameter("nameIN", c.getName());
            spq.setParameter("newratingIN", r.getRating());
            spq.setParameter("usernameIN", u.getUsername());

            spq.execute();
            return Boolean.TRUE;

        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }

}
    
    public static List<Integer> getAllRatingByCharacter(Model.Character c){
        List<Integer> returnList = new ArrayList<>();
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("get_all_rating_by_character");
            
            spq.registerStoredProcedureParameter("nameIN", String.class, ParameterMode.IN);
            spq.setParameter("nameIN", c.getName());
            

            List<Object[]> objectList = spq.getResultList();
            
           for(Object[] object : objectList){
                Integer a = Integer.parseInt(object[0].toString());
                returnList.add(a);
       }

        return returnList;

        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return returnList;
        }

    }
    public static List<Integer> getAllRaingByMovie(Movie m){
        List<Integer> returnList = new ArrayList<>();
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("get_all_rating_by_movie");
            
            spq.registerStoredProcedureParameter("nameIN", String.class, ParameterMode.IN);
            spq.setParameter("nameIN", m.getTitle());
            

            List<Object[]> objectList = spq.getResultList();
            for(Object[] object : objectList){
                Integer a = Integer.parseInt(object[0].toString());
                returnList.add(a);
        }

        return returnList;

        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return returnList;
        }

    }
     public static List<Integer> getAllRaingBySeries(Series s){
        List<Integer> returnList = new ArrayList<>();
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("get_all_rating_by_series");
            
            spq.registerStoredProcedureParameter("nameIN", String.class, ParameterMode.IN);
            spq.setParameter("nameIN", s.getTitle());
            

            List<Object[]> objectList = spq.getResultList();
            for(Object[] object : objectList){
                Integer a = Integer.parseInt(object[0].toString());
                returnList.add(a);
        }

        return returnList;

        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return returnList;
        }

    }
    
    
    
    
}
