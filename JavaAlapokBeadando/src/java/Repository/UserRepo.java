
package Repository;

import Model.Database;
import Model.Movie;
import Model.User;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author Bjakushiki
 */
public class UserRepo {
    
      public static Boolean addNewUser(User u){
        try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("create_user");
           
            spq.registerStoredProcedureParameter("usernameIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("passwdIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("emailIN", String.class, ParameterMode.IN);
            
            spq.setParameter("usernameIN", u.getUsername());
            spq.setParameter("passwdIN", u.getPassword());
            spq.setParameter("emailIN", u.getEmail());
            
            
            spq.execute();
            return Boolean.TRUE;
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }
        
    }
      public static Boolean deleteUser(User u){
        try{
            EntityManager em = Database.getdbConn();
            
            StoredProcedureQuery spq = em.createStoredProcedureQuery("delete_user_by_name");
            
            spq.registerStoredProcedureParameter("usernameIN", String.class, ParameterMode.IN);
            
            spq.setParameter("usernameIN", u.getUsername());
            
            spq.execute();
            return Boolean.TRUE;
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }
      }
      
       public static Boolean updateUser(User u){
           try{
            EntityManager em = Database.getdbConn();

            StoredProcedureQuery spq = em.createStoredProcedureQuery("update_user");
            
            spq.registerStoredProcedureParameter("usernameIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("passwdIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("emailIN", String.class, ParameterMode.IN);
            
            spq.setParameter("usernameIN", u.getUsername());
            spq.setParameter("passwdIN", u.getPassword());
            spq.setParameter("emailIN", u.getEmail());
            
            
            spq.execute();
            return Boolean.TRUE;
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }
       }
       public static User getUserByEmail(User u){
             User reutrnUser = new User(0,"nincs","nincs","nincs",1);
        try{  
            EntityManager em = Database.getdbConn();
            
            StoredProcedureQuery spq = em.createStoredProcedureQuery("get_user_by_email");
            
            spq.registerStoredProcedureParameter("emailIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("nameOUT", String.class, ParameterMode.OUT);
            
            spq.setParameter("usernameIN", u.getUsername());

            spq.execute();
            
            reutrnUser.setUsername(spq.getOutputParameterValue("nameOUT").toString());
            
            return reutrnUser;
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return reutrnUser;
        }
      }
      public static User getUserByUsername(User u){
             User reutrnUser = new User(0,"nincs","nincs","nincs",1);
        try{
            EntityManager em = Database.getdbConn();
            
            StoredProcedureQuery spq = em.createStoredProcedureQuery("get_user_id_by_name");
            
            spq.registerStoredProcedureParameter("nameIN", String.class, ParameterMode.IN);
            spq.registerStoredProcedureParameter("useridOUT", Integer.class, ParameterMode.OUT);
            
            spq.setParameter("nameIN", u.getUsername());

            spq.execute();
            
            reutrnUser.setId(Integer.parseInt(spq.getOutputParameterValue("useridOUT").toString()));
            
            return reutrnUser;
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return reutrnUser;
        }
      }
       
    
}
